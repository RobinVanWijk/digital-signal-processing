import DSP_io as io
import DSP_filters as filters
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal as sg


# Import sound file
# Read wav file for analysis
x = io.readwav('single.wav')
# x = io.readwav('multiple.wav')
# x = io.readwav('sound.wav')
nchannels = x[0]
samplerate = x[1]
samplewidth = x[2]
Nfrms = x[3]
time = x[4]
data = x[5]
ND = len(data)
Ts = 1/samplerate

if nchannels == 2:
    dataL = [data[i] for i in range(ND) if i % 2 == 1]
    dataR = [data[i] for i in range(ND) if i % 2 == 0]

print('ch,Fs,sw,length:', x[0:4])

# FFT
t = np.arange(Nfrms)*Ts


# take subset of Data
strt = 0
step = int(len(data) / nchannels)  # number of intervals
stp = strt+step
intervals = stp-strt
subDatR = dataR[strt:stp]
subDatL = dataL[strt:stp]

## BUTTERWORTH

## Variables ##
butt_n_l = 4  # order
butt_n_r = 4

butt_type_l = 'bandpass'
butt_type_r = 'bandpass'

butt_freq_l = [4500,10000]
butt_freq_r = [4500,10000]

amp = 3
fs_l = 2*samplerate
fs_r = samplerate

butt_string_l = 'Butt %s filter f=%a, n=%d, amp=%d' % (butt_type_l, butt_freq_l, butt_n_l, amp)
butt_string_r = 'Butt %s filter f=%a, n=%d, amp=%d' % (butt_type_r, butt_freq_r, butt_n_r, amp)

# find coefficients
coef_b_l, coef_a_l = sg.butter(butt_n_l, butt_freq_l, btype=butt_type_l, fs=fs_l)
coef_b_r, coef_a_r = sg.butter(butt_n_r, butt_freq_r, btype=butt_type_r, fs=fs_r)

print('Rb: ', coef_b_r)
print('Ra: ', coef_a_r)
print('Lb: ', coef_b_l)
print('La: ', coef_a_l)

r_a = np.array(coef_a_r)
r_b = np.array(coef_b_r)  # b0,b1,...bm

l_a = np.array(coef_a_l)
l_b = np.array(coef_b_l)  # b0,b1,...bm

butt_yR = filters.IIR_filter(subDatR, r_a, r_b)
butt_yL = filters.IIR_filter(subDatL, l_a, l_b)



##Create filtered .wav file
#stereo audio
dataToWrite = [0] * Nfrms * nchannels
j = 0
for i in range(Nfrms):
    dataToWrite[j] = amp*np.array(butt_yL[i])
    dataToWrite[j+1] = amp*np.array(butt_yL[i])
    j += 2

#write to disk
io.writewave(butt_string_l+'.wav', samplerate, samplewidth,
             Nfrms * nchannels / samplerate, dataToWrite)



##Create filtered .wav file
#stereo audio
dataToWrite = [0] * Nfrms * nchannels
j = 0
for i in range(Nfrms):
    dataToWrite[j] = amp*np.array(butt_yR[i])
    dataToWrite[j+1] = amp*np.array(butt_yR[i])
    j += 2

#write to disk
io.writewave(butt_string_r+'.wav', samplerate, samplewidth,
             Nfrms * nchannels / samplerate, dataToWrite)



# Plots
plt.style.use('dark_background')
fig, (axs) = plt.subplots(1, 2, figsize=(16, 9))
fig.canvas.set_window_title('DSP - Analysis & Filtering')
plt.subplots_adjust(hspace=1.5, wspace=0.15, left=0.1,
                    right=0.9, top=0.95, bottom=0.05)



# FFT
t = np.arange(Nfrms)*Ts


# take subset of Data
strt = 0
step = int(len(data) / nchannels)  # number of intervals
stp = strt+step
intervals = stp-strt
subDatR = butt_yR[strt:stp]
subDatL = butt_yL[strt:stp]




# Butterworth Filter(R)
strt = 0
stp = int(np.trunc(Nfrms))
axs[0].plot(time[strt:stp], dataR[strt:stp], 'r',
               time[strt:stp], amp*np.array(butt_yR[strt:stp]), 'b')
axs[0].set_title(butt_string_r)
axs[0].legend(['signal', 'filtered'])
axs[0].set(xlabel='time [s]')
axs[0].set(ylabel='Amplitude u(t) ')
axs[0].grid(True)

# Butterworth Filter(L)
strt = 0
stp = int(np.trunc(Nfrms))
axs[1].plot(time[strt:stp], dataL[strt:stp], 'r',
               time[strt:stp], amp*np.array(butt_yL[strt:stp]), 'b')
axs[1].set_title(butt_string_l)
axs[1].legend(['signal', 'filtered'])
axs[1].set(xlabel='time [s]')
axs[1].set(ylabel='Amplitude u(t) ')
axs[1].grid(True)


plt.show()