import DSP_io as io
import DSP_filters as filters
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal as sg

# Import sound file
# Read wav file for analysis
x = io.readwav('single.wav')
# x = io.readwav('multiple.wav')
#x = io.readwav('sound.wav')
nchannels = x[0]
samplerate = x[1]
samplewidth = x[2]
Nfrms = x[3]
time = x[4]
data = x[5]
ND = len(data)
Ts = 1/samplerate

if nchannels == 2:
    dataL = [data[i] for i in range(ND) if i % 2 == 1]
    dataR = [data[i] for i in range(ND) if i % 2 == 0]

print('ch,Fs,sw,length:', x[0:4])
print('Filtering....')
# FFT
t = np.arange(Nfrms)*Ts

# take DFT and scale results
sp = 2/Nfrms*np.fft.fft(data)
spR = 2/Nfrms*np.fft.fft(dataR)
spL = 2/Nfrms*np.fft.fft(dataL)
freq = np.fft.fftfreq(Nfrms, Ts)

# take subset of Data
strt = 0
step = int(len(data) / nchannels)  # number of intervals
stp = strt+step
intervals = stp-strt
subDatR = dataR[strt:stp]
subDatL = dataL[strt:stp]


## Pre filtered data
# apply windowing before FFT
windowedSubDatR = filters.window(subDatR)
windowedSubDatL = filters.window(subDatL)

windowedSPR = abs(2/intervals*np.fft.fft(windowedSubDatR))  # z, y=i, for log scale
windowedSPL = abs(2/intervals*np.fft.fft(windowedSubDatL))  # z, y=i, for log scale
# only first half <Fs/2 contains valid data
kR = int(np.trunc(len(windowedSPR)/2))
kL = int(np.trunc(len(windowedSPL)/2))
windowedSPR = windowedSPR[0:kR]
windowedSPL = windowedSPL[0:kL]
windowedFreq = np.fft.fftfreq(intervals, Ts)  # x
windowedFreqR = windowedFreq[0:kR]
windowedFreqL = windowedFreq[0:kL]
windowedT = np.arange(step)*Ts
windowedDataR = windowedSubDatR
windowedDataL = windowedSubDatL

## BUTTERWORTH FILTER ##
## Variables ##
butt_n = 6  # order
butt_type = 'bandpass'
butt_freq = [4000,11000]
amp = 5

butt_string = 'Butt %s filter f=%a, n=%d, amp=%d' % (butt_type, butt_freq, butt_n, amp)

# find coefficients
b, a = sg.butter(butt_n, butt_freq, btype=butt_type, fs=samplerate)

butt_yR = filters.IIR_filter(subDatR, a, b)
butt_yL = filters.IIR_filter(subDatL, a, b)

butt_n = 6  # order
butt_type = 'bandstop'
butt_freq = [11000,17000]

butt_string = 'Butt %s filter f=%a, n=%d, amp=%d' % (butt_type, butt_freq, butt_n, amp)
b, a = sg.butter(butt_n, butt_freq, btype=butt_type, fs=samplerate)

butt_yR = filters.IIR_filter(butt_yR, a, b)
butt_yL = filters.IIR_filter(butt_yL, a, b)

butt_n = 6  # order
butt_type = 'bandstop'
butt_freq = [1000,4000]

butt_string = 'Butt %s filter f=%a, n=%d, amp=%d' % (butt_type, butt_freq, butt_n, amp)
b, a = sg.butter(butt_n, butt_freq, btype=butt_type, fs=samplerate)

butt_yR = filters.IIR_filter(butt_yR, a, b)
butt_yL = filters.IIR_filter(butt_yL, a, b)

## Filtered data
# take subset of Data
windowedSubDatRFiltered = amp*np.array(butt_yR[strt:stp])
windowedSubDatLFiltered = amp*np.array(butt_yL[strt:stp])

windowedSubDatRFiltered = filters.window(windowedSubDatRFiltered)
windowedSubDatLFiltered = filters.window(windowedSubDatLFiltered)

resultSPR = abs(2/intervals*np.fft.fft(windowedSubDatRFiltered))  # z, y=i, for log scale
resultSPL = abs(2/intervals*np.fft.fft(windowedSubDatLFiltered))  # z, y=i, for log scale
# only first half <Fs/2 contains valid data
kR = int(np.trunc(len(resultSPR)/2))
kL = int(np.trunc(len(resultSPL)/2))
resultSPR = resultSPR[0:kR]
resultSPL = resultSPL[0:kL]
resultFreq = np.fft.fftfreq(intervals, Ts)  # x
resultFreqR = resultFreq[0:kR]
resultFreqL = resultFreq[0:kL]
resultT = np.arange(step)*Ts
resultDataR = windowedSubDatR
resultDataL = windowedSubDatL

##Create filtered .wav file
#stereo audio
dataToWrite = [0] * Nfrms * nchannels
j = 0
for i in range(Nfrms):
    dataToWrite[j] = amp*np.array(butt_yL[i])
    dataToWrite[j+1] = amp*np.array(butt_yR[i])
    j += 2

#write to disk
io.writewave(butt_string+'.wav', samplerate, samplewidth,
             Nfrms * nchannels / samplerate, dataToWrite)
# Plots
plt.style.use('dark_background')
fig, (axs) = plt.subplots(4, 2, figsize=(16, 9))
fig.canvas.set_window_title('DSP - Analysis & Filtering')
plt.subplots_adjust(hspace=1.5, wspace=0.15, left=0.1,
                    right=0.9, top=0.95, bottom=0.05)

# FFT windowed data Plot(R)
axs[0, 0].plot(windowedT, windowedDataR)
axs[0, 0].set_title('FFT Windowed Data (R)')
axs[0, 0].set(xlabel='time [s]')
axs[0, 0].set(ylabel='Amplitude u(t) ')

# FFT windowed data Plot(L)
axs[0, 1].plot(windowedT, windowedDataL)
axs[0, 1].set_title('FFT Windowed Data (L)')
axs[0, 1].set(xlabel='time [s]')
axs[0, 1].set(ylabel='Amplitude u(t) ')

# FFT windowed spectrum - Pre filter  Plot(R)
axs[1, 0].plot(windowedFreqR, abs(windowedSPR), '^r')
axs[1, 0].set(yscale='log')
axs[1, 0].set_title('FFT Windowed Spectrum - Pre filter (R)')
axs[1, 0].set(xlabel='Frequency [Hz]')
axs[1, 0].set(ylabel='Amplitude F ')

# FFT windowed spectrum - Pre filter  Plot(L)
axs[1, 1].plot(windowedFreqL, abs(windowedSPL), '^r')
axs[1, 1].set(yscale='log')
axs[1, 1].set_title('FFT Windowed Spectrum - Pre filter (L)')
axs[1, 1].set(xlabel='Frequency [Hz]')
axs[1, 1].set(ylabel='Amplitude F ')

# IIR Filter(R)
strt = 0
stp = int(np.trunc(Nfrms))
axs[2, 0].plot(time[strt:stp], dataR[strt:stp], 'r',
               time[strt:stp], amp*np.array(butt_yR[strt:stp]), 'b')
axs[2, 0].set_title(butt_string)
axs[2, 0].legend(['signal', 'filtered'], loc='upper right')
axs[2, 0].set(xlabel='time [s]')
axs[2, 0].set(ylabel='Amplitude u(t) ')
axs[2, 0].grid(True)

# IIR Filter(L)
strt = 0
stp = int(np.trunc(Nfrms))
axs[2, 1].plot(time[strt:stp], dataL[strt:stp], 'r',
               time[strt:stp], amp*np.array(butt_yL[strt:stp]), 'b')
axs[2, 1].set_title(butt_string)
axs[2, 1].legend(['signal', 'filtered'], loc='upper right')
axs[2, 1].set(xlabel='time [s]')
axs[2, 1].set(ylabel='Amplitude u(t) ')
axs[2, 1].grid(True)

# FFT windowed spectrum - Post filter  Plot(R)
axs[3, 0].plot(resultFreqR, abs(resultSPR), '^r')
axs[3, 0].set(yscale='log')
axs[3, 0].set_title('FFT Windowed Spectrum - Post filter (R)')
axs[3, 0].set(xlabel='Frequency [Hz]')
axs[3, 0].set(ylabel='Amplitude F ')

# FFT windowed spectrum - Post filter  Plot(L)
axs[3, 1].plot(resultFreqL, abs(resultSPL), '^r')
axs[3, 1].set(yscale='log')
axs[3, 1].set_title('FFT Windowed Spectrum - Post filter  (L)')
axs[3, 1].set(xlabel='Frequency [Hz]')
axs[3, 1].set(ylabel='Amplitude F ')
plt.show()