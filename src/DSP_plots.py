import matplotlib
import matplotlib.pyplot as plt
import numpy as np

## graphs is an array of [xdata, ydata]-arrays
def plot_graphs(graphs):
    fig, (axs) = plt.subplots(2, 3, figsize=(12, 8))
    fig.suptitle("DSP filtering")
    plt.subplots_adjust(hspace = 0.5)


    #### Upper left plot
    ## Graphs
    axs[0, 0].plot(graphs[0][0], graphs[0][1], label = 'signal')
    axs[0, 1].plot(graphs[0][0], graphs[0][1], label = 'signal')

    #### Lower left plot
    ## Graphs
    axs[1, 0].plot(graphs[3][0], graphs[3][1], label = 'signal')

    plt.show()