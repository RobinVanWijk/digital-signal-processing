import numpy as np
import matplotlib.pyplot as plt

def IIR_filter(x,a,b):      #general IIR filter
    Nsamples=len(x)
    y=[0]*Nsamples
    Na=len(a)
    Nb=len(b)
    for i in range(Nb,Nsamples):
        sumbx=0
        for j in range(0,Nb):
            if i-j>=0:
                sumbx+=b[j]*x[i-j]
        sumay=0
        for k in range(1,Na):
            if i-k>=0:
                sumay+=a[k]*y[i-k]
        y[i]=(sumbx-sumay)/a[0]
    return(y)        

def win_Ham(n,Ns):      #Hamming windowing function (LI Tan, Digital Signal Processing, ch 7.3 p.230)
    w=0.54-0.46*np.cos(2*np.pi*n/Ns)
    return(w)

def window(x):      #general IIR filter
    Nsamples=len(x)
    y=[0]*Nsamples
    for i in range(Nsamples):
        y[i]=win_Ham(i,Nsamples)*x[i]

    return(y)